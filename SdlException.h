//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_SDLEXCEPTION_H
#define SUDOKU_SDLEXCEPTION_H


#include <stdexcept>

class SdlException : public std::runtime_error {
private:
    std::string sdlError_;
    static std::string make_what(const char* sdlError);
public:
    SdlException();
    std::string getSdlError() const;
};


#endif //SUDOKU_SDLEXCEPTION_H
