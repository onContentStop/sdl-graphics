//
// Created by kyle on 8/31/19.
//

#include "SdlException.h"
#include <SDL2/SDL_quit.h>

using std::string;

string SdlException::make_what(const char *sdlError) {
    return string("SDL function failed: ") + sdlError;
}

SdlException::SdlException()
    : runtime_error(make_what(SDL_GetError())), sdlError_(SDL_GetError()) {}

string SdlException::getSdlError() const {
    return sdlError_;
}
