//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_SIZEDFONT_H
#define SUDOKU_SIZEDFONT_H

#include <SDL2/SDL_ttf.h>
#include <map>
#include <string>
class SizedFont {
private:
    std::string fontFile_;
    std::map<int, TTF_Font*> fontSizes_;

public:
    explicit SizedFont(const std::string& fontFile);
    ~SizedFont();
    TTF_Font* getFont(int size);
    SizedFont();
};

#endif // SUDOKU_SIZEDFONT_H
