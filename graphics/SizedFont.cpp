//
// Created by kyle on 8/31/19.
//

#include "SizedFont.h"
#include "../TtfException.h"
SizedFont::SizedFont(const std::string &fontFile) : fontFile_(fontFile) {
    if (fontFile.empty()) {
	throw std::runtime_error("Font file must be provided");
    }
}
SizedFont::~SizedFont() {
    for (auto &font : fontSizes_) {
	TTF_CloseFont(font.second);
    }
}
TTF_Font *SizedFont::getFont(int size) {
    auto it = fontSizes_.find(size);
    if (it != fontSizes_.end()) {
        return it->second;
    } else {
        TTF_Font* font = TTF_OpenFont(fontFile_.c_str(), size);
        if (font == nullptr) {
            throw TtfException();
        }
        fontSizes_.insert({size, font});
        return font;
    }
}
SizedFont::SizedFont() = default;
