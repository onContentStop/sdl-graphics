//
// Created by kyle on 8/31/19.
//

#ifndef SUDOKU_SCREEN_H
#define SUDOKU_SCREEN_H

#include "../elements/Element.h"
#include <SDL2/SDL_render.h>
#include <vector>
class Screen {
protected:
    std::vector<Element *> elements_;
    std::function<void(SDL_Event &)> eventHandler;
public:
    Screen() = default;
    ~Screen();
    virtual void draw(SDL_Renderer *renderer) = 0;
    void handleEvent(SDL_Event &event);
};

#endif // SUDOKU_SCREEN_H
